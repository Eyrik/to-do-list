import { createStore } from "vuex";

export const store = createStore({
    state: {
        tasks: [],
        tasksDone: []   
    },
    getters: {
        getTasks(state) {
            return state.tasks
        },
        getTasksDone(state) {
            return state.tasksDone
        }
    },
    mutations: {
        SET_TASKS(state, tasks) {
            state.tasks = tasks
        },
        SET_TASKS_DONE(state, tasksDone) {
            state.tasksDone = tasksDone
        },

    }
})